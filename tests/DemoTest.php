<?php

namespace App\Tests;

use App\Entity\Demo;
use PHPUnit\Framework\TestCase;

class DemoTest extends TestCase
{
    public function testDemo(): void
    {
        $demo = new Demo();

        $demo->setDdemo('demo');

        $this->assertTrue($demo->getDdemo() === 'demo');
    }
}
