<?php

namespace App\Entity;

use App\Repository\DemoRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DemoRepository::class)]
class Demo
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $ddemo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDdemo(): ?string
    {
        return $this->ddemo;
    }

    public function setDdemo(string $ddemo): self
    {
        $this->ddemo = $ddemo;

        return $this;
    }
}
